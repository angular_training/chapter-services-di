import { Component, OnInit } from '@angular/core';
import { TasksService } from './services/tasks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  //providers: [TasksService]
})
export class AppComponent implements OnInit {

  tasks: {title: string, status: string}[] = [];

  ngOnInit() {
    this.tasks = this.tasksService.tasks;
  }

  constructor(private tasksService: TasksService) {}

  onTaskAdded(newTask: {title: string, status: string}) {
    this.tasksService.addTask(newTask);
  }

  onStatusChanged(updateInfo: {id: number, newStatus: string}) {
    this.tasksService.updateTaskStatus(updateInfo.id, updateInfo.newStatus);
  }

}
