import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoggingService } from '../services/logging.service';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  //providers: [LoggingService]
  //providers: [LoggingService, TasksService]
})
export class TaskComponent implements OnInit {

  @Input() task: {title: string, status: string};
  @Input() id: number;

  // constructor(private logger: LoggingService, private tasksService: TasksService) { }

  constructor(private tasksService: TasksService) { }

  ngOnInit() {
  }

  onSetTo(status: string) {
    this.tasksService.updateTaskStatus(this.id, status);
    // Never proceed like this with services in an angular
    // const logger = new LoggingService ();
    //this.logger.logStatusChange(status);
  }

}
