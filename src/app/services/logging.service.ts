export class LoggingService {
  logStatusChange(status: string) {
    console.log('A task status changed, new status: ' + status);
  }
}