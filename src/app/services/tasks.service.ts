import { LoggingService } from './logging.service';
import { Injectable } from '@angular/core';

@Injectable()
export class TasksService {

  tasks = [
    {
      title: 'Master Angular',
      status: 'open'
    },
    {
      title: 'Learn NodeJs',
      status: 'in progress'
    },
    {
      title: 'Getting started GraphQL',
      status: 'archived'
    }
  ];

  constructor(private logger: LoggingService) {}

  addTask(task: {title: string, status: string}) {
    this.tasks.push(task);
    this.logger.logStatusChange(status);
  }

  updateTaskStatus(id: number, newStatus: string) {
    this.tasks[id].status = newStatus;
    this.logger.logStatusChange(newStatus);
  }
}