import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { TasksService } from './services/tasks.service';
import { LoggingService } from './services/logging.service';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    NewTaskComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [TasksService, LoggingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
