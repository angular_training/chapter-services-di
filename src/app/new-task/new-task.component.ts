import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoggingService } from '../services/logging.service';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css'],
  //providers: [LoggingService]
  //providers: [LoggingService, TasksService]
})
export class NewTaskComponent implements OnInit {

  @Output() taskAdded = new EventEmitter<{title: string, status: string}>();
  
  //constructor(private logger: LoggingService, private tasksService: TasksService) { }
  constructor(private tasksService: TasksService) { }

  ngOnInit() {}


  onCreateTask(taskTitle: string, taskStatus: string) {
    this.tasksService.addTask({title: taskTitle, status: taskStatus});
    //this.logger.logStatusChange(taskStatus);
  }

}
